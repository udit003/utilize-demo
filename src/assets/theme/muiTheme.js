import { createMuiTheme } from "@material-ui/core/styles";
import { purple, pink, grey } from "@material-ui/core/colors";

const color = "rgba(255, 255, 255, 0.7)";
export const textColor = grey[800];

const theme = createMuiTheme({
    typography: {
        useNextVariants: true,
        fontFamily: [
            "Nunito",
            "Roboto",
            "Open Sans",
            '"Helvetica Neue"',
            "Arial",
            "sans-serif",
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"'
        ].join(",")
    },
    palette: {
        primary: {
            light: purple[50],
            main: purple[500],
            dark: purple[600],
            contrastText: "#fff"
        },
        secondary: {
            light: pink[300],
            main: pink[500],
            dark: pink[600],
            contrastText: "#fff"
        }
    },
    shape: {
        borderRadius: 8
    },
    shadows: [
        "none",
        "0 1px 4px 0 rgba(0, 0, 0, 0.14)",
        "1px 2px 7px 0px rgba(0,0,0,0.14)",
        "0 6px 13px -2px rgba(0,0,0,0.14)",
        "0 8px 16px -1px rgba(0,0,0,0.14)",
        "0px 9px 19px 2px rgba(0,0,0,0.14)"
    ]
});

export default theme;
