import React from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import { Box } from "@material-ui/core";

const useStyles = makeStyles(() =>
    createStyles({
        img: {
            width: "100%",
            maxWidth: 404,
            margin: "2rem",
        },
    })
);


const NotFound: React.FC = () => {

    const classes = useStyles();

    return (
        <img className={classes.img} src="../../assets/404.png" alt="404 image" />
    );
}

export default NotFound;