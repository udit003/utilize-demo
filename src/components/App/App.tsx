import React from 'react';
import logo from './logo.svg';
import './App.css';
import LandingPage from '../../routes/landing-page/landing-page';
import {AlertSnackbarComp} from '../alert-snackbar';
import { Switch, Redirect, Route } from 'react-router-dom';
import HomePage from '../../routes/home-page/home-page';
import NotFound from '../404/404';

type IProp = {
  isLoggedIn: boolean
}

const App:React.FC<IProp> = ({isLoggedIn}) => {
  return (
    <div>
        <Switch>
          <Route exact path="/home">
              {isLoggedIn ? <HomePage /> :<Redirect to="/" />}
          </Route>
          <Route exact path="/">
            {isLoggedIn ? <Redirect to="/home" /> : <LandingPage />}
          </Route>
          <Route>
            <NotFound />
          </Route>
        </Switch>
       <AlertSnackbarComp />
    </div>
  );
}

export default App;
