import { connect } from "react-redux";
import { RootState } from "../../redux/reducers";
import App from "./App";


const mapStateToProps = (state:RootState) => {
    return {
        isLoggedIn: state.AppState.isLoggedIn
    }
}

export default connect(mapStateToProps)(App);