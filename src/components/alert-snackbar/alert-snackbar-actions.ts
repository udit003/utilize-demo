export enum AlertMessageType {
    SUCCESS_MESSAGE = "ALERT_SUCCESS_MESSAGE",
    INFO_MESSAGE = "ALERT_INFO_MESSAGE",
    ERROR_MESSAGE = "ALERT_ERROR_MESSAGE",
    WARNING_MESSAGE = "ALERT_WARNING_MESSAGE"
}

export type AlertSnackbarAction = {
    type : AlertMessageType,
    message : string
}


export function dispatchAlertAction(dispatch:Function,alertType:AlertMessageType,msg:string){
    dispatch({
        type: alertType,
        message:msg
    })
}
