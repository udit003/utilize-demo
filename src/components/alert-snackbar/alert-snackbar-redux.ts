
import {connect} from 'react-redux';
import AlertSnackbar from './alert-snackbar';

const mapStateToProps = (state:any) => {
    //console.log("NEW STATE RECEIVED ",state)
    return {
        alertType : state.AlertSnackbar.alertType,
        message : state.AlertSnackbar.message,
        lastCreatedAt : state.AlertSnackbar.lastCreatedAt
    }
}

export default connect(mapStateToProps)(AlertSnackbar);