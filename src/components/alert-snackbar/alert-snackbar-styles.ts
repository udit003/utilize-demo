import {makeStyles,createStyles,Theme} from "@material-ui/core/styles";
import { amber, green } from '@material-ui/core/colors';

export const useSnackbarContentStyles = makeStyles((theme: Theme) =>
createStyles({
    ALERT_SUCCESS_MESSAGE: {
        backgroundColor: green[600],
    },
    ALERT_ERROR_MESSAGE: {
        backgroundColor: theme.palette.error.dark,
    },
    ALERT_INFO_MESSAGE: {
        backgroundColor: theme.palette.primary.main,
    },
    ALERT_WARNING_MESSAGE: {
        backgroundColor: amber[700],
    },
    icon: {
        fontSize: 20,
    },
    iconVariant: {
        opacity: 0.9,
        marginRight: theme.spacing(1),
    },
    message: {
        display: 'flex',
        alignItems: 'center',
    }
})
);


export const useSnackbarStyles = makeStyles((theme: Theme) => 
createStyles({
    margin: {
      margin: theme.spacing(1),
    },
  })
);