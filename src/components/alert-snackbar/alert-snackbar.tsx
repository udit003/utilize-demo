import React, {SyntheticEvent} from 'react';
import {Snackbar,SnackbarContent,IconButton} from '@material-ui/core';
import clsx from 'clsx';
import {AlertMessageType} from './alert-snackbar-actions';
// this snackbar has a open and a close and it needs to be 
import WarningIcon from '@material-ui/icons/Warning';
import InfoIcon from '@material-ui/icons/Info';
import ErrorIcon from '@material-ui/icons/Error';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import CloseIcon from '@material-ui/icons/Close';
import {useSnackbarContentStyles,useSnackbarStyles} from './alert-snackbar-styles';

const variantIcon = {
    ALERT_SUCCESS_MESSAGE : CheckCircleIcon,
    ALERT_WARNING_MESSAGE: WarningIcon,
    ALERT_ERROR_MESSAGE: ErrorIcon,
    ALERT_INFO_MESSAGE: InfoIcon,
};  

interface ContentCompProps {
    message?: string;
    onClose?: () => void;
    variant: keyof typeof variantIcon;
}

function MySnackbarContentWrapper(props: ContentCompProps) {
    const classes = useSnackbarContentStyles();
    const { message, onClose, variant, ...other } = props;
    const Icon = variantIcon[variant];
  
    return (
      <SnackbarContent
        className={classes[variant]}
        aria-describedby="client-snackbar"
        message={
          <span id="client-snackbar" className={classes.message}>
            <Icon className={clsx(classes.icon, classes.iconVariant)} />
            {message}
          </span>
        }
        action={[
          <IconButton key="close" aria-label="close" color="inherit" onClick={onClose}>
            <CloseIcon className={classes.icon} />
          </IconButton>,
        ]}
        {...other}
      />
    );
  }

type IProp = {
    alertType?:AlertMessageType,
    message?:string,
    lastCreatedAt?:number
}

// this is a pure component, now I will connect this component to redux 
const AlertSnackbar:React.FC<IProp> = ({alertType,message,lastCreatedAt}) => {
    const classes = useSnackbarStyles();
    const [open, setOpen] = React.useState(alertType !== undefined);

    const handleClose = (event?: SyntheticEvent, reason?: string) => {
      //console.log("On Close called")
        if (reason === 'clickaway') {
          return;
        }
    
        setOpen(false);
    };

    //{console.log("OPEN_STATUS ",open)}

    React.useEffect(() => {
      //console.log("Use Effect called")
      if(open==false)
        // and your props are not
      setOpen(alertType !== undefined);
    },[alertType,message,lastCreatedAt])
    // prop wahi hai

    return (
        <Snackbar
            anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
            }}
            open={open}
            autoHideDuration={6000}
            onClose={handleClose}
        > 
        
             <MySnackbarContentWrapper
                onClose={handleClose}
                variant={alertType || AlertMessageType.SUCCESS_MESSAGE}
                message={message}
        />  
        </Snackbar>
    
    );
}

// I will definitely be needed to 

export default AlertSnackbar;