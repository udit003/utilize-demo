import Authentication from "./authentication";
import { connect } from "react-redux";
import { UserProfileInfo } from "../../redux/reducers/app-state";
import { UserLoginAction } from "../../redux/actions/account-action";
import { dispatchAlertAction, AlertMessageType } from "../alert-snackbar/alert-snackbar-actions";


const createUserLoginAction = (userProfileInfo:UserProfileInfo):UserLoginAction => {
    return {type:'USER_LOGIN',userProfileInfo}
}

const mapDispatchToProps = (dispatch: Function) => {
    return {
        onLoginSuccess: (userProfileInfo:UserProfileInfo) => {
            dispatch(createUserLoginAction(userProfileInfo))
            dispatchAlertAction(dispatch,AlertMessageType.SUCCESS_MESSAGE,"You are successfully LoggedIn")
        },
        onLoginFailed: () => {

        }
    }
}

export default connect(null,mapDispatchToProps)(Authentication);