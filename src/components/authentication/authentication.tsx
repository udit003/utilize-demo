import React from "react";
import GoogleLoginComp from "./google-login/google-login";
import { UserProfileInfo } from "../../redux/reducers/app-state";


type IProp = {
    onLoginSuccess: (userProfileInfo:UserProfileInfo) => void,
    onLoginFailed: () => void
}

const Authentication:React.FC<IProp> = ({onLoginFailed,onLoginSuccess}) => {

    const handleGoogleLoginSuccess = (res:any) => {
        console.log("Inside google login Success")
        onLoginSuccess({loginMethod:'GOOGLE',associatedId: res.profileObj.googleId ,photoUrl:res.profileObj.imageUrl, email: res.profileObj.email,userName:res.profileObj.name});
        console.log("LoginSuccess",res)
    }
    
    const handleGoogleLoginFailure = (res:any) => {
        onLoginFailed();
        console.log("LoginFailed",res)
    }

    return (
        <GoogleLoginComp handleLoginFailure={handleGoogleLoginFailure} handleLoginSuccess={handleGoogleLoginSuccess}/>    
    )
}


export default Authentication;