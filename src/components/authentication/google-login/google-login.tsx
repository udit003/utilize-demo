import React from "react";
import { GoogleLogin } from 'react-google-login';

const clientId = '555484091920-l06usodcmvkm1pdl5gvami6jfjpuhug0.apps.googleusercontent.com';


type IProp = {
    handleLoginSuccess: (res:any) => void,
    handleLoginFailure: (res:any) => void
}

const GoogleLoginComp:React.FC<IProp> = ({handleLoginFailure,handleLoginSuccess}) => {

    return (
       <div>
           <GoogleLogin
                clientId={clientId}
                buttonText="Login"
                onSuccess={handleLoginSuccess}
                onFailure={handleLoginFailure}
                cookiePolicy={'single_host_origin'}
                isSignedIn={true}
            />
       </div>
    )
}


export default GoogleLoginComp;