import React from 'react';
import ReactDOM, { render } from 'react-dom';
import './index.css';
import {AppComp} from './components/App';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import {MuiThemeProvider} from '@material-ui/core';
import MuiTheme from './assets/theme/muiTheme';
import {createStore, applyMiddleware } from 'redux'; 
import { rootReducer } from './redux/reducers';
import { createLogger } from 'redux-logger';

function startApp({store}:any) {
  render(
    <Provider store={store}>
      <BrowserRouter forceRefresh={false}>
        <MuiThemeProvider theme={MuiTheme}>
          <AppComp />
        </MuiThemeProvider>
      </BrowserRouter>
    </Provider>,
    document.getElementById('root'),
  );
}

const middleware: Array<any> = []
if(process.env.NODE_ENV !== 'production') {
  middleware.push(createLogger());
}


function initializeStore() {
  console.log("Creating new Redux Store");
  const store = createStore(rootReducer,applyMiddleware(...middleware))
  return store;
}

function main(){
  console.log("Inside App Main function");
  const store = initializeStore();
  startApp({store});
}

// start the app
main();


