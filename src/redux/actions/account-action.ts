import { UserProfileInfo } from "../reducers/app-state"

export type UserLoginAction = {
    type: 'USER_LOGIN',
    userProfileInfo: UserProfileInfo
}

export type UserLogoutAction = {
    type: 'USER_LOGOUT'
}
