import { OrderObj } from "../../utils/core-types/order-type"

export type CreateOrderAction = {
    type: 'CREATE_NEW_ORDER',
    newOrderObj: OrderObj
}

export type DeleteOrderAction = {
    type: 'DELETE_ORDER',
    orderId: string
}

export type ModifyOrderAction = {
    type: 'MODIFY_ORDER',
    modifiedOrderObj: OrderObj
}