import { AlertMessageType, AlertSnackbarAction } from "../../components/alert-snackbar/alert-snackbar-actions";

type AlertSnackbarState = {
    alertType?: AlertMessageType,
    message?: string,
    lastCreatedAt?:number
};

export default function alertSnackbar( state:AlertSnackbarState = {}, action:AlertSnackbarAction ): AlertSnackbarState {
  switch (action.type) {
    case AlertMessageType.SUCCESS_MESSAGE :
    case AlertMessageType.ERROR_MESSAGE : 
    case AlertMessageType.WARNING_MESSAGE :
    case AlertMessageType.INFO_MESSAGE : 
      return {alertType:action.type, message:action.message,lastCreatedAt:Date.now()}
    default:
      return state;
  }
}