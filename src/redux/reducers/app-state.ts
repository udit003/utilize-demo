import { UserLoginAction, UserLogoutAction } from "../actions/account-action"

export type AppData = {
    appReady: boolean, //this will help me put the spinner for the whole app anytime I need it. Like put the entire app in waiting state.
    isLoggedIn: boolean,
    userProfileInfo? : UserProfileInfo
}

export type UserProfileInfo = {
    loginMethod: string,
    userName: string,
    photoUrl: string,
    email: string,
    associatedId: string
}

export const AppState = (state: AppData = {appReady:true,isLoggedIn: false}, action: UserLoginAction | UserLogoutAction ):AppData => {
    switch (action.type) {
        case 'USER_LOGIN': {
            return {appReady: true,isLoggedIn:true,userProfileInfo: action.userProfileInfo}
        }

        case 'USER_LOGOUT': {
            return {appReady: true,isLoggedIn: false,userProfileInfo: undefined}
        }

        default:
            return state
    }
}