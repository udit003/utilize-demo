import { combineReducers } from "redux";

import { AppState } from './app-state';
import {OrdersState} from './orders-state';
import AlertSnackbar from './alert-snackbar-reducer';

export const rootReducer = combineReducers({
    AppState,
    OrdersState,
    AlertSnackbar
})


export type RootState = ReturnType<typeof rootReducer>