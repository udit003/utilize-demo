import { OrderObj, ProductType } from "../../utils/core-types/order-type"
import { UserLoginAction, UserLogoutAction } from "../actions/account-action"
import { defaultOrderList } from "../../utils/data/mock-data"
import { CreateOrderAction,DeleteOrderAction,ModifyOrderAction } from "../actions/order-action"


const getDefaultOrderList = () : OrderObj[]=> {
    
    return defaultOrderList.map((jsonOrderObj) => {
        return {
            orderId: jsonOrderObj.id,
            custEmail: jsonOrderObj.customer_email,
            custName: jsonOrderObj.customer_name,
            productQty: jsonOrderObj.quantity,
            productType: jsonOrderObj.product
        }
    })
}

export const OrdersState = (state: OrderObj[] = getDefaultOrderList(),action:CreateOrderAction | DeleteOrderAction | ModifyOrderAction | UserLoginAction | UserLogoutAction ) => {
    switch(action.type) {
        
        case 'USER_LOGIN': {
            return getDefaultOrderList()
        }

        case 'USER_LOGOUT': {
            return []
        }

        case 'CREATE_NEW_ORDER' :
            return [action.newOrderObj,...state]

        case 'DELETE_ORDER' : {
            let newList = state.filter(orderObj => {
                return orderObj.orderId !== action.orderId
            })
            return newList
        }

        case 'MODIFY_ORDER' : {
            let newList = state.map(orderObj => {
                if(orderObj.orderId === action.modifiedOrderObj.orderId)
                    return action.modifiedOrderObj
                return orderObj
            })
            return newList
        }
            
        default :
            return state
    }
}