export type OrderObj = {
    orderId: string,
    custName: string,
    custEmail: string,
    productType: ProductType,
    productQty: number
}


export type ProductType = "Product 1" | "Product 2" | "Product 3" 

export type JsonOrderObj = {
    id: string,
    customer_name: string,
    customer_email: string,
    product: ProductType ,
    quantity: number
}


